import { TransactIoMsg } from './lib/TransactIoMsg';

export { TransactIoError } from './lib/TransactIoError';
export { TransactIoErrorCode } from './lib/TransactIoErrorCode';
export { TransactIoMsg } from './lib/TransactIoMsg';
export { ITransactJWT } from './lib/TransactToken';

export function newTransactIoMsg(): TransactIoMsg {
  return new TransactIoMsg();
}
