export type TransactResponseMethod = 'CLOSE' | 'GET' | 'POST';
export type TransactTokenClass = 'PROD' | 'DEV';
export declare class ITransactJWT {
    meta: {
        [key: string]: number | string;
    };
    method: TransactResponseMethod;
    price: number;
    tclass: TransactTokenClass;
    title: string;
    url: string;
    img: string;
    item: string;
    uid: string;
    bid: number;
    rid: number;
    iat: number;
    tid: string;
    sub: boolean;
    sub_expires: number;
}
