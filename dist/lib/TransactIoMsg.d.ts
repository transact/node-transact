import { ITransactJWT } from './TransactToken';
/**
 * @description Transact.io message handler
 *
 * @class TransactIoMsg
 */
export declare class TransactIoMsg {
    /**
     *
     *
     * @param {string} token
     * @param {any} callback
     * @returns
     * @memberof TransactIoMsg
     */
    static verify(token: string, secret: string, callback?: (err: any, decoded: ITransactJWT) => void): Promise<object>;
    static isFromTransactServer(token: ITransactJWT): boolean;
    private token;
    private secret;
    constructor();
    setMethodPost(): void;
    setMethodClose(): void;
    setMethodGet(): void;
    setClassProduction(): void;
    setClassTest(): void;
    /**
     *
     *
     * @param {string} secret Your secret key to sign the message
     * @memberof TransactIoMsg
     */
    setSecret(secret: string): void;
    /**
     *
     *
     * @param {string} url address of purchased item
     * @memberof TransactIoMsg
     */
    setURL(url: string): void;
    /**
     *
     *
     * @param {string} url address of purchased item
     * @memberof TransactIoMsg
     */
    isSubscription(isSubscription: boolean): void;
    /**
     *
     *
     * @param {string} title name of what they are buying.
     * @memberof TransactIoMsg
     */
    setTitle(title: string): void;
    /**
     *
     *
     * @param {number} price in cents
     * @memberof TransactIoMsg
     */
    setPrice(price: number): void;
    /**
     *
     *
     * @param {string} item code to idntify the item they are buying
     * @memberof TransactIoMsg
     */
    setItem(item: string): void;
    /**
     *
     *
     * @param {string} item code to idntify the item they are buying
     * @memberof TransactIoMsg
     */
    setThumnailImageUrl(url: string): void;
    /**
     *
     *
     * @param {string} uid Unique transaction ID to track on your site.
     * @memberof TransactIoMsg
     */
    setUid(uid: string): void;
    /**
     *
     *
     * @param {number} recipient user ID on transact
     * @memberof TransactIoMsg
     */
    setRecipient(recipient: number): void;
    /**
     * @description (Optional)  Force the ID of the buyer.
     * If you don't specify it will be whoever logs in on transact.io
     * If you DO specify, payment will only be accepted from this buyer ID
     *
     * @param {number} buyerID User ID on transact, who is buying
     * @memberof TransactIoMsg
     */
    setBuyer(buyerID: number): void;
    /**
     *
     *
     * @param {[key: string]: number | string} meta
     * @memberof TransactIoMsg
     */
    setMeta(meta: {
        [key: string]: number | string;
    }): void;
    setMetaKeyValue(key: string, value: number | string): void;
    /**
     * @description Check for presense of fields that transact server
     * sets.  NOTE you still must call verify()
     *
     * @param {number} buyerID User ID on transact, who is buying
     * @memberof TransactIoMsg
     */
    isFromTransactServer(decoded: ITransactJWT): boolean;
    /**
     *
     *
     * @param {(err, t: string) => void} [callback]
     * @returns
     * @memberof TransactIoMsg
     */
    getToken(callback?: (err: Error, signature: string) => void): Promise<string>;
}
