import { TransactIoErrorCode } from './TransactIoErrorCode';
export declare class TransactIoError extends Error {
    message: string;
    code: TransactIoErrorCode;
    err: any;
    constructor(msg: string, code: TransactIoErrorCode, err: any);
}
