export declare enum TransactIoErrorCode {
    NONE = 0,
    ERROR = 1,
    INVALID_SECRET = 2,
    INVALID_KEY = 3,
    PRICE_TOO_LOW = 4,
    SIGNING_ERROR = 5,
    VALIDATION_ERROR = 6,
    INVALID_RECIPIENT = 7,
    INVALID_TITLE = 8,
    SIGNATURE_TOO_BIG = 9
}
