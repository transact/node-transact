"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactIoErrorCode = void 0;
var TransactIoErrorCode;
(function (TransactIoErrorCode) {
    TransactIoErrorCode[TransactIoErrorCode["NONE"] = 0] = "NONE";
    TransactIoErrorCode[TransactIoErrorCode["ERROR"] = 1] = "ERROR";
    TransactIoErrorCode[TransactIoErrorCode["INVALID_SECRET"] = 2] = "INVALID_SECRET";
    TransactIoErrorCode[TransactIoErrorCode["INVALID_KEY"] = 3] = "INVALID_KEY";
    TransactIoErrorCode[TransactIoErrorCode["PRICE_TOO_LOW"] = 4] = "PRICE_TOO_LOW";
    TransactIoErrorCode[TransactIoErrorCode["SIGNING_ERROR"] = 5] = "SIGNING_ERROR";
    TransactIoErrorCode[TransactIoErrorCode["VALIDATION_ERROR"] = 6] = "VALIDATION_ERROR";
    TransactIoErrorCode[TransactIoErrorCode["INVALID_RECIPIENT"] = 7] = "INVALID_RECIPIENT";
    TransactIoErrorCode[TransactIoErrorCode["INVALID_TITLE"] = 8] = "INVALID_TITLE";
    TransactIoErrorCode[TransactIoErrorCode["SIGNATURE_TOO_BIG"] = 9] = "SIGNATURE_TOO_BIG";
})(TransactIoErrorCode = exports.TransactIoErrorCode || (exports.TransactIoErrorCode = {}));
