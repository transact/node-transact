"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactIoMsg = void 0;
var jwt = require('jsonwebtoken');
var TransactIoError_1 = require("./TransactIoError");
var TransactIoErrorCode_1 = require("./TransactIoErrorCode");
var TransactToken_1 = require("./TransactToken");
/**
 * @description Transact.io message handler
 *
 * @class TransactIoMsg
 */
var TransactIoMsg = /** @class */ (function () {
    function TransactIoMsg() {
        this.token = new TransactToken_1.ITransactJWT();
        this.setMethodClose();
        this.setClassProduction();
    }
    /**
     *
     *
     * @param {string} token
     * @param {any} callback
     * @returns
     * @memberof TransactIoMsg
     */
    TransactIoMsg.verify = function (token, secret, callback) {
        return new Promise(function (resolve, reject) {
            var jwtOptions = {
                algorithm: 'HS256',
            };
            if (!secret || secret.length < 1) {
                var transactIoError = new TransactIoError_1.TransactIoError('Invalid secret', TransactIoErrorCode_1.TransactIoErrorCode.INVALID_SECRET, null);
                if (callback) {
                    callback(transactIoError, null);
                }
                else {
                    reject(transactIoError);
                }
                return;
            }
            var jwtToken = jwt.verify(token, secret, jwtOptions, callback ? callback : undefined);
            resolve(jwtToken);
        });
    };
    TransactIoMsg.isFromTransactServer = function (token) {
        if (token.tid) {
            return true;
        }
        return false;
    };
    TransactIoMsg.prototype.setMethodPost = function () {
        this.token.method = 'POST';
    };
    TransactIoMsg.prototype.setMethodClose = function () {
        this.token.method = 'CLOSE';
    };
    TransactIoMsg.prototype.setMethodGet = function () {
        this.token.method = 'GET';
    };
    TransactIoMsg.prototype.setClassProduction = function () {
        this.token.tclass = 'PROD';
    };
    TransactIoMsg.prototype.setClassTest = function () {
        this.token.tclass = 'DEV';
    };
    /**
     *
     *
     * @param {string} secret Your secret key to sign the message
     * @memberof TransactIoMsg
     */
    TransactIoMsg.prototype.setSecret = function (secret) {
        this.secret = secret;
    };
    /**
     *
     *
     * @param {string} url address of purchased item
     * @memberof TransactIoMsg
     */
    TransactIoMsg.prototype.setURL = function (url) {
        this.token.url = url;
    };
    /**
     *
     *
     * @param {string} url address of purchased item
     * @memberof TransactIoMsg
     */
    TransactIoMsg.prototype.isSubscription = function (isSubscription) {
        this.token.sub = isSubscription;
    };
    /**
     *
     *
     * @param {string} title name of what they are buying.
     * @memberof TransactIoMsg
     */
    TransactIoMsg.prototype.setTitle = function (title) {
        this.token.title = title;
    };
    /**
     *
     *
     * @param {number} price in cents
     * @memberof TransactIoMsg
     */
    TransactIoMsg.prototype.setPrice = function (price) {
        this.token.price = price;
    };
    /**
     *
     *
     * @param {string} item code to idntify the item they are buying
     * @memberof TransactIoMsg
     */
    TransactIoMsg.prototype.setItem = function (item) {
        this.token.item = item;
    };
    /**
     *
     *
     * @param {string} item code to idntify the item they are buying
     * @memberof TransactIoMsg
     */
    TransactIoMsg.prototype.setThumnailImageUrl = function (url) {
        this.token.img = url;
    };
    /**
     *
     *
     * @param {string} uid Unique transaction ID to track on your site.
     * @memberof TransactIoMsg
     */
    TransactIoMsg.prototype.setUid = function (uid) {
        this.token.uid = uid;
    };
    /**
     *
     *
     * @param {number} recipient user ID on transact
     * @memberof TransactIoMsg
     */
    TransactIoMsg.prototype.setRecipient = function (recipient) {
        this.token.rid = recipient;
    };
    /**
     * @description (Optional)  Force the ID of the buyer.
     * If you don't specify it will be whoever logs in on transact.io
     * If you DO specify, payment will only be accepted from this buyer ID
     *
     * @param {number} buyerID User ID on transact, who is buying
     * @memberof TransactIoMsg
     */
    TransactIoMsg.prototype.setBuyer = function (buyerID) {
        this.token.bid = buyerID;
    };
    /**
     *
     *
     * @param {[key: string]: number | string} meta
     * @memberof TransactIoMsg
     */
    TransactIoMsg.prototype.setMeta = function (meta) {
        this.token.meta = meta;
    };
    TransactIoMsg.prototype.setMetaKeyValue = function (key, value) {
        if (!this.token.meta) {
            this.token.meta = {};
        }
        this.token.meta.key = value;
    };
    /**
     * @description Check for presense of fields that transact server
     * sets.  NOTE you still must call verify()
     *
     * @param {number} buyerID User ID on transact, who is buying
     * @memberof TransactIoMsg
     */
    TransactIoMsg.prototype.isFromTransactServer = function (decoded) {
        if (this.token.tid) {
            return true;
        }
        return false;
    };
    /**
     *
     *
     * @param {(err, t: string) => void} [callback]
     * @returns
     * @memberof TransactIoMsg
     */
    TransactIoMsg.prototype.getToken = function (callback) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var jwtOptions = {
                algorithm: 'HS256',
            };
            var transactIoError = null;
            if (!_this.secret || _this.secret.length < 1) {
                transactIoError = new TransactIoError_1.TransactIoError('Invalid secret', TransactIoErrorCode_1.TransactIoErrorCode.INVALID_SECRET, null);
            }
            if (_this.token.price === null || _this.token.price < 0
                || _this.token.price === undefined || isNaN(_this.token.price)) {
                transactIoError = new TransactIoError_1.TransactIoError('Invalid Price', TransactIoErrorCode_1.TransactIoErrorCode.PRICE_TOO_LOW, null);
            }
            if (!_this.token.rid || isNaN(_this.token.rid)) {
                transactIoError = new TransactIoError_1.TransactIoError('Invalid recipient ID', TransactIoErrorCode_1.TransactIoErrorCode.INVALID_RECIPIENT, null);
            }
            if (!_this.token.title || _this.token.title.length < 1) {
                transactIoError = new TransactIoError_1.TransactIoError('Invalid title', TransactIoErrorCode_1.TransactIoErrorCode.INVALID_RECIPIENT, null);
            }
            if (transactIoError != null) {
                if (callback) {
                    callback(transactIoError, null);
                }
                else {
                    reject(transactIoError);
                }
                return;
            }
            _this.token.iat = Date.now();
            var token = Object.assign({}, _this.token);
            jwt.sign(token, _this.secret, jwtOptions, function (err, signature) {
                if (err || !signature) {
                    transactIoError = new TransactIoError_1.TransactIoError('Signing error', TransactIoErrorCode_1.TransactIoErrorCode.SIGNING_ERROR, err);
                    if (callback) {
                        callback(transactIoError, null);
                    }
                    else {
                        reject(transactIoError);
                    }
                    return;
                }
                if (signature.length > 900) {
                    transactIoError = new TransactIoError_1.TransactIoError('Signing error: too big', TransactIoErrorCode_1.TransactIoErrorCode.SIGNATURE_TOO_BIG, null);
                    if (callback) {
                        callback(transactIoError, null);
                    }
                    else {
                        reject(transactIoError);
                    }
                    return;
                }
                if (callback) {
                    callback(null, signature);
                }
                else {
                    resolve(signature);
                }
            });
        });
    };
    return TransactIoMsg;
}());
exports.TransactIoMsg = TransactIoMsg;
