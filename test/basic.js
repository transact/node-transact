var assert = require('assert');
var transactIo = require('../dist/index');

describe('transact-payment', function() {
	describe('TransactIoMsg.getToken', function() {
		it('should fail with error no price', function(done) {
			var msg = new transactIo.TransactIoMsg();
			var promise = msg.getToken();

			promise
				.then(function (token) {
					done('token created without price')
				})
				.catch(function (error) {
					done()
				});
		});

		it('should fail with error invalid price', function(done) {
			var msg = new transactIo.TransactIoMsg();
			msg.setPrice('non-numeric');
			var promise = msg.getToken();

			promise
				.then(function (token) {
					done('token created with invalid price')
				})
				.catch(function (error) {
					done()
				});
		});

		it('should fail with error no secret', function(done) {
			var msg = new transactIo.TransactIoMsg();
			msg.setPrice(111);
			var promise = msg.getToken();

			promise
				.then(function (token) {
					done('token created without secret')
				})
				.catch(function (error) {
					done()
				});
		});

		it('should fail with error no recipiend id', function(done) {
			var msg = new transactIo.TransactIoMsg();
			msg.setPrice(111);
			msg.setSecret('test');
			var promise = msg.getToken();

			promise
				.then(function (token) {
					done('token created without secret')
				})
				.catch(function (error) {
					done()
				});
		});

		it('should fail with error no title', function(done) {
			var msg = new transactIo.TransactIoMsg();
			msg.setPrice(111);
			msg.setSecret('test');
			msg.setRecipient(22);
			var promise = msg.getToken();

			promise
				.then(function (token) {
					done('token created without secret')
				})
				.catch(function (error) {
					done()
				});
		});

		it('should create a token', function(done) {
			var msg = new transactIo.TransactIoMsg();
			msg.setPrice(111);
			msg.setSecret('test');
			msg.setRecipient(22);
			msg.setTitle('test title');
			var promise = msg.getToken();

			promise
				.then(function (token) {
					done()
				})
				.catch(function (error) {
					done(error)
				});
		});

		it('should create a token and verify it', function(done) {
			var secret = 'random-secret';
			var msg = new transactIo.TransactIoMsg();
			msg.setPrice(111);
			msg.setSecret(secret);
			msg.setRecipient(22);
			msg.setTitle('test title');
			var promise = msg.getToken();

			promise
				.then(function (token) {

					var verifyPromise = transactIo.TransactIoMsg.verify(token,secret);
					verifyPromise
						.then(function () {
							done();
						})
						.catch(function (error) {
							done(error);
						});
				})
				.catch(function (error) {
					done(error)
				});
		});

		it('should fail verifying the token with invalid secret', function(done) {
			var secret = 'random-secret';
			var msg = new transactIo.TransactIoMsg();
			msg.setPrice(111);
			msg.setSecret(secret);
			msg.setRecipient(22);
			msg.setTitle('test title');
			var promise = msg.getToken();

			promise
				.then(function (token) {
					var invalidSecret = 'random-invalid-secret';
					var verifyPromise = transactIo.TransactIoMsg.verify(token,invalidSecret);
					verifyPromise
						.then(function () {
							done('Token verified with an invalid secret');
						})
						.catch(function (error) {
							done();
						});
				})
				.catch(function (error) {
					done(error)
				});
		});

	});
});
