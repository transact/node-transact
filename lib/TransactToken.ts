export type TransactResponseMethod = 'CLOSE' | 'GET' | 'POST';
export type TransactTokenClass = 'PROD' | 'DEV';

export class ITransactJWT {
    // data only in request
    public meta: {[key: string]: number | string}; // meta data you wish to store
    public method: TransactResponseMethod; // CLOSE == close the window, other possible is POST
    public price: number; // price in cents
    public tclass: TransactTokenClass;  // token class,  default to prod
    public title: string; // user readable title of purchase
    public url: string; // URL purchased also gets messaged passed to it.
    public img: string; // Image thumbnail URL 4:3 aspect ratio


    // common to both request and response
    public item: string; // item code for what is being purchase
    public uid: string; // unique ID set by seller to track payment
    public bid: number; // buyer user ID optional in request
    public rid: number; // recipient ID, who receives funds in response
    public iat: number; // issued at part of JWT spec


    // only in response below
    public tid: string; // transaction ID on transact.io
    public sub: boolean; // subscribed to publisher
    public sub_expires: number; //expiration date timestamp
}
