export enum TransactIoErrorCode {
  NONE = 0,
  ERROR = 1, // generic error
  INVALID_SECRET = 2,
  INVALID_KEY = 3,
  PRICE_TOO_LOW = 4,
  SIGNING_ERROR = 5,  // ERROR From lower layer jwt
  VALIDATION_ERROR = 6,  // ERROR From lower layer jwt
  INVALID_RECIPIENT = 7,
  INVALID_TITLE = 8,
  SIGNATURE_TOO_BIG = 9, // we limit to 900 bytes since URL must be under 1K
}
