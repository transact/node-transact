import { TransactIoErrorCode } from './TransactIoErrorCode';

export class TransactIoError extends Error {
  public message = '';
  public code: TransactIoErrorCode;
  public err: any;

  constructor(msg: string, code: TransactIoErrorCode, err: any) {
    super();
    this.message = msg;
    this.code = code;
  }
}
