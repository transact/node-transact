## transact-payments

This is a library for use with [transact.io](https://transact.io/)

See an example on package [transact-payments-demo](https://www.npmjs.com/package/transact-payments-demo) with a live demo

## Working with this package
```
Add transact-payments package to your package.json file
```

## Usage

Require transact-payments library

```javascript
let transactIo = require('transact-payments');
```

Instantiate a new instance of the TransactIoMsg class

```javascript
let transactToken = new transactIo.TransactIoMsg(); // get new instance
```


**Required:** Set the secret that is in the Developer Settings / Keys 
of the transact publisher menu
```javascript
transactToken.setSecret('Signing Secret'); // secret key
```


**Required:** Set the Account ID of who recieves the funds
```
transactToken.setRecipient('5206507264147456'); // Account ID to pay
````


**Required:** set the URL of what is being purchased.   
```javascript
// Note that the host name MUST match the real name for cross site 
// messaging to work. 
transactToken.setURL($_REQUEST['url']);
```

**Required:**  Set the price in pences of the sale
```javascript
transactToken.setPrice(2);
```

**Required**:  Set an item or product code.   This can be something
unique to the article or content you are selling.   The buyer 
will see this as part of the name of the charge
```javascript
transactToken.setItem('ItemCode1'); // item code
```

**Optional**:  Set a Unique ID associated with this sale.  
```javascript
transactToken.setUid('UniqueSaleID');
````


**Optional**:  Set meta data you want to save with this sale.  
```javascript
// Set your own meta data
// Note you must keep this short to avoid going over the 1024 byte limt
// of the token URL
transactToken.setMeta({
	'your' : 'data',
	'anything' : 'you want'
});
```

### Get token for normal purchase

Get a token that will be passed to transact.js after all paramateres are set.
There are two ways to generate a token.

Using callback
```javascript
function getTokenCb(error,token) {
	if (error) {
		console.log("Failed to create the token with error:",error);
	} else {
		console.log("Token created successfully:",token);
	}
}

transactToken.getToken(getTokenCb);
```


Using promise
```javascript
let promise = transactToken.getToken();

promise.then(function(token) {
	console.log("Token created successfully:",token);
).catch(function(error) {
	console.log("Failed to create the token with error:",error);
});
```

### Verifying a token

To verify a token the signing secret needs to match the one used when creating the token.
There are two ways to verify a token.

Using callback
```javascript
let transactIo = require('transact-payments');
let token = 'token generated previously';
let secret = 'Signing Secret';

function tokenVerification(error,verifiedToken) {
	if (error) {
		console.log("Failed to verify the token with error:",error);
	} else {
		console.log("Token verified successfully:",verifiedToken);
	}
}

transactIo.TransactIoMsg.verify(token,secret,tokenVerification);
```

Using promise
```javascript
let transactIo = require('transact-payments');
let token = 'token generated previously';
let secret = 'Signing Secret';

let promise = transactIo.TransactIoMsg.verify(token,secret);
promise.then(function(verifiedToken) {
	console.log("Token verified successfully:",verifiedToken);
}).catch(function(error) {
	console.log("Failed to verify the token with error:",error);
});
```
-------
